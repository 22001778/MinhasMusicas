import UIKit

class DetalheMusicaViewController: UIViewController {

    var nomeImagem: String = ""
    var nomeMusica: String = ""
    var nomeAlbum: String = ""
    var nomeCantor: String = ""
    
    
    @IBOutlet weak var capa: UIImageView!
    @IBOutlet weak var musica: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var cantor: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.capa.image = UIImage(named: self.nomeImagem)
        self.musica.text = self.nomeMusica
        self.album.text = self.nomeAlbum
        self.cantor.text = self.nomeCantor

    }


}
